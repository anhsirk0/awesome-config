awk '
{
    if (NF == 3) print "color."$2"_"$3" = \""$1"\"";
    else if (NF == 2) print "color."$2" = \""$1"\"";
}
' ./colors
